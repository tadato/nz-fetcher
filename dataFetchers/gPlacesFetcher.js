"use strict"
const request = require('superagent-bluebird-promise');
const Promise = require('bluebird');

module.exports = function(lat, lon) {
	let irrelevantTypes = [];
	let placesCount = new Map();

	return request.get('https://maps.googleapis.com/maps/api/place/nearbysearch/json')
		.query({
			key: 'AIzaSyBwnGWcCBAdMtM2h69aHpgiSKAD0S8PI8g',
			radius: '30',
			location: `${lat}, ${lon}`,
			rankby: 'prominence' 
		})
		.then(extractRelevantPlacesFromResponse)
		.then(() => {
			let placesData = {};

			for (let [type, count] of placesCount) {
				placesData[type] = count;
			}

			return placesData
		})

	function extractRelevantPlacesFromResponse(res) {
		for (let place of res.body.results) {
			for (let type of place.types.filter(type => !irrelevantTypes.includes(type))) {
				if (placesCount.has(type))
					placesCount.set(type, placesCount.get(type) + 1)
				else
					placesCount.set(type, 1);
			}
		}
		
		if (res.body.next_page_token)
			return loopRequest(res.body.next_page_token);
		else
			return Promise.resolve();
	}

	function loopRequest(pagetoken) {
		return new Promise((res, rej) => {
			setTimeout(res, 2000);
		})
		.then(() => {
			return request.get('https://maps.googleapis.com/maps/api/place/nearbysearch/json')
			.query({
				pagetoken,
				key: 'AIzaSyBwnGWcCBAdMtM2h69aHpgiSKAD0S8PI8g',
				radius: '30',
				location: `${lat}, ${lon}`,
				rankby: 'prominence' 
			})
		})
		.then((res) => {
			return extractRelevantPlacesFromResponse(res);
		})
	};
}