#!/usr/bin/env node

/**
 * Module dependencies.
 */
const program = require('commander');
const fetch = require('../index');

console.log('parsing');
program
	.version('0.0.1')
	.arguments('<nz>')
	.action(function (nz) {
		let lat, lon;
		lat = nz.split(',')[0]
		lon = nz.split(',')[1]
		console.log(`parsing ${lat}, ${lon}`);
		return fetch(lat, lon)
			.then(() => {
				console.log('finished parsing');
			})
			.catch(err => {
				console.error('error parsing lat, lon', err);
			})
	});
	
program.parse(process.argv);