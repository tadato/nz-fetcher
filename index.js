const low = require('lowdb');
const fileAsync = require('lowdb/lib/storages/file-async');
const Promise = require('bluebird');
const populationFetcher = require("./dataFetchers/populationFetcher");
const gPlacesFetcher = require("./dataFetchers/gPlacesFetcher");

const db = low('db.json', {
  storage: fileAsync
})

module.exports = function(lat, lon) {
	return Promise.join(
		populationFetcher(lat, lon),
		gPlacesFetcher(lat, lon),
		(popData, gPlacesData) => {
			return db.get('locations')
				.remove({ lat, lon })
				.write()
				.then(() => {
					return db.get('locations')
						.push({
							lat,
							lon,
							populationData: popData,
							placesData: gPlacesData
						})
						.write();
				})
		}
	)
}